import { Routes } from '@angular/router';
import { CalculatorComponent } from './components/calculator/calculator/calculator.component';
import { HeroesComponent } from './components/heroes/heroes/heroes.component';
import { beersGuard } from './guards/beers.guard';

export const routes: Routes = [
    { path: 'calculator', component: CalculatorComponent },
  { path: 'heroes', component: HeroesComponent},
  { path: 'apod',  loadComponent: () => import('./components/apod/apod/apod.component').then(mod => mod.ApodComponent) },
  { path: 'beers', loadComponent: () => import('./components/beers/beers/beers.component').then(mod => mod.BeersComponent), canActivate: [beersGuard]},
  { path: 'beers/:id', loadComponent: () => import('./components/beers/beer-details/beer-details.component').then(mod => mod.BeerDetailsComponent)},
  { path: 'forms', loadComponent: () => import('./components/forms/forms/forms.component').then(mod => mod.FormsComponent) },
//   { path: 'starwars', loadComponent: () => import('./components/sw/sw/sw.component').then(mod => mod.SwComponent) },
  { path: '', redirectTo: '/beers', pathMatch: 'full'},
  { path: '**', redirectTo: '/beers', pathMatch: 'full'},

];
