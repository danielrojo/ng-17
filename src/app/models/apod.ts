export class Apod {

    private _date = ''
    private _explanation = '';
    private _hdurl = '';
    private _media_type = '';
    private _service_version = '';
    private _title = '';
    private _url = '';
    private _isImage = false;
    private _isVideo = false;
    private _videoId = '';
    private _isEmpty = true;

    constructor(json?: any) {
        if (json === undefined) {
            return;
        }
        this._date = json.date;
        this._explanation = json.explanation;
        json.hdurl !== undefined ? this._hdurl = json.hdurl : this._hdurl = '';
        this._media_type = json.media_type;
        this._service_version = json.service_version;
        this._title = json.title;
        this._url = json.url;
        this._isImage = this._media_type === 'image';
        this._isVideo = this._media_type === 'video';
        if (this._isVideo) {
            let result = this.youtubeParser(this._url);
            if (typeof result !== 'boolean') {
                this._videoId = result;
            }
        }
        this._isEmpty = false;
    }

    get date() {
        return this._date;
    }

    get explanation() {
        return this._explanation;
    }

    get hdurl() {
        return this._hdurl;
    }

    get mediaType() {
        return this._media_type;
    }

    get serviceVersion() {
        return this._service_version;
    }

    get title() {
        return this._title;
    }

    get url() {
        return this._url;
    }

    get isImage() {
        return this._isImage;
    }

    get isVideo() {
        return this._isVideo;
    }

    get videoId() {
        return this._videoId;
    }

    get isEmpty() {
        return this._isEmpty;
    }

    youtubeParser(url: string): string | boolean {
        var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/;
        var match = url.match(regExp);
        return (match&&match[7].length==11)? match[7] : false;
    }

}