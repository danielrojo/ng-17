import { Pipe, PipeTransform } from '@angular/core';
import { Beer } from '../models/beer';
import { Observable, filter, from, of } from 'rxjs';

@Pipe({
  name: 'filterBeersPipe',
  standalone: true,
  pure: true,
})
export class FilterBeersPipe implements PipeTransform {

  transform(value: Beer[], ...args: number[]): Observable<Beer[]> {

    // return filtered beers by abv range in a new observable
    return of(value.filter((beer: Beer) => beer.abv >= args[0] && beer.abv <= args[1]));

  }

}
