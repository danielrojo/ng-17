import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'abv',
  standalone: true,
  pure: true,
})
export class AbvPipe implements PipeTransform {

  transform(value: number, ...args: unknown[]): string {
    console.log('Pipe: ' + value);
    
    if (args[0] === 'º') return value + 'º';
    return value + '%';
  }

}
