import { Component, OnInit } from '@angular/core';
import { BeersService } from '../../../services/beers.service';
import { Beer } from '../../../models/beer';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSliderModule, Options } from '@angular-slider/ngx-slider';
import { filter, from } from 'rxjs';
import { AsyncPipe, CurrencyPipe } from '@angular/common';
import { AbvPipe } from '../../../pipes/abv.pipe';
import { FilterBeersPipe } from '../../../pipes/filter-beers.pipe';
import { Router, RouterModule } from '@angular/router';

@Component({
  selector: 'app-beers',
  standalone: true,
  imports: [NgbPaginationModule, NgxSliderModule, AbvPipe, FilterBeersPipe, AsyncPipe, RouterModule],
  templateUrl: './beers.component.html',
  styleUrl: './beers.component.scss',
})
export class BeersComponent implements OnInit {

  beers: Beer[] = [];

  lowValue: number = 4;
  highValue: number = 5;
  options: Options = {
    floor: 0,
    ceil: 70,
    step: 0.1,
  };

  constructor(private service: BeersService) {}

  ngOnInit() {
    this.service.beers$.subscribe((data: any) => {
      this.beers = data;
    });
    this.service.getBeers();
  }

}

