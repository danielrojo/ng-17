import { Component, OnInit } from '@angular/core';
import { BeersService } from '../../../services/beers.service';
import { ActivatedRoute } from '@angular/router';
import { Beer } from '../../../models/beer';

@Component({
  selector: 'app-beer-details',
  standalone: true,
  imports: [],
  templateUrl: './beer-details.component.html',
  styleUrl: './beer-details.component.scss'
})
export class BeerDetailsComponent implements OnInit {
  beer: Beer | undefined = new Beer();

  constructor(private service: BeersService, private activated : ActivatedRoute) { 

  }
  ngOnInit(): void {
    this.activated.params.subscribe( params => {
      // find the beer by id
      this.service.beers$.subscribe((data: any) => {
        this.beer = data.find((beer: Beer) => beer.id === +params['id']);
      });
      
    });
    this.service.beers$.subscribe((data: any) => {
      if (data.length === 0) {
        this.service.getBeers();
      }
    });
  }

}
