import { Component, OnInit } from '@angular/core';
import { ApodService } from '../../../services/apod.service';
import { Apod } from '../../../models/apod';
import { JsonPipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { YouTubePlayer } from '@angular/youtube-player';
import { ApodInfoComponent } from '../apod-info/apod-info.component';
import { ApodPickerComponent } from '../apod-picker/apod-picker.component';
import moment from 'moment';

@Component({
  selector: 'app-apod',
  standalone: true,
  imports: [JsonPipe, YouTubePlayer, ApodInfoComponent, ApodPickerComponent],
  templateUrl: './apod.component.html',
  styleUrl: './apod.component.scss',
})
export class ApodComponent implements OnInit {
  apod = new Apod();

  // today's date using momentjs format 'YYYY-MM-DD'
  dateStr = moment().format('YYYY-MM-DD');

  constructor() {}

  ngOnInit(): void {
  }

  datePicked(dateStr: string){
    console.log('handleDate');
    // date as a string with format 'YYYY-MM-DD'
    this.dateStr = dateStr;
  }
}
