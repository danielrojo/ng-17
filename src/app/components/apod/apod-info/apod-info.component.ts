import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { YouTubePlayer } from '@angular/youtube-player';
import { Apod } from '../../../models/apod';
import moment from 'moment';
import { ApodService } from '../../../services/apod.service';

@Component({
  selector: 'app-apod-info',
  standalone: true,
  imports: [YouTubePlayer],
  templateUrl: './apod-info.component.html',
  styleUrl: './apod-info.component.scss'
})
export class ApodInfoComponent implements OnInit, OnChanges{

  @Input() apod: Apod = new Apod();
  @Input() dateStr: string = moment().format('YYYY-MM-DD');

  constructor(private service: ApodService) { }

  ngOnInit(): void {
    this.service.apod$.subscribe((apod) => {
      this.apod = apod;
    });

    this.service.getApod();
  }

  ngOnChanges(changes: SimpleChanges): void {
    // check if dateStr has changed from previous value
    console.log(JSON.stringify(changes));
    
    if ( changes['dateStr'] && changes['dateStr'].currentValue !== changes['dateStr'].previousValue ) {
      this.service.getApod(this.dateStr);
    }
  }

}
