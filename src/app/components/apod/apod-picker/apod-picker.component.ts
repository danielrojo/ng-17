import { Component, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';

//import momentjs
import moment from 'moment';

@Component({
  selector: 'app-apod-picker',
  standalone: true,
  imports: [NgbDatepickerModule, FormsModule],
  templateUrl: './apod-picker.component.html',
  styleUrl: './apod-picker.component.scss'
})
export class ApodPickerComponent {

  @Output() onDateChange = new EventEmitter<any>();
  // date for today using momentjs
  date: any = {year: moment().year(), month: moment().month() + 1, day: moment().date()};

  handleDate(){
    console.log('handleDate');
    let dateStr = `${this.date.year}-${this.date.month}-${this.date.day}`;
    this.onDateChange.emit(dateStr);
  }
}
