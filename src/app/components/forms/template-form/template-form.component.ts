import { Component } from '@angular/core';
import { Hero } from '../../../models/hero';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-template-form',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './template-form.component.html',
  styleUrl: './template-form.component.scss',
})
export class TemplateFormComponent {

  model = new Hero('', '');
  submitted = false;

  // name pattern for min 3 characters and only alphabets and space allowed, max 20 characters, allow spanish characters
  namePattern = '^[a-zA-ZñÑáéíóúÁÉÍÓÚçÇ ]{3,20}$';
  
  onSubmit() {
    this.submitted = true;
    console.log(this.model);
    
  }
  
  newHero() {
    this.model = new Hero('', '');
  }
}
