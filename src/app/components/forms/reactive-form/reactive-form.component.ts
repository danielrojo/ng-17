import { JsonPipe } from '@angular/common';
import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';

function validateName(control: AbstractControl) : any|null {
  // if first letter is not uppercase return error
  let result: any = null;
  control.value.charAt(0) !== control.value.charAt(0).toUpperCase() ? result = { invalidName: true } : result = null;
  return result;
}


@Component({
  selector: 'app-reactive-form',
  standalone: true,
  imports: [FormsModule, ReactiveFormsModule, JsonPipe],
  templateUrl: './reactive-form.component.html',
  styleUrl: './reactive-form.component.scss',
})
export class ReactiveFormComponent {
  profileForm = this.fb.group({
    name: ['', [Validators.required, validateName]],
    description: ['una descripcion'],
  });

  constructor(private fb: FormBuilder) {}
  updateProfile() {
    this.profileForm.patchValue({
      name: 'Nancy',
      description: 'Drew',

    });
  }
  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.profileForm.value);
  }
}
