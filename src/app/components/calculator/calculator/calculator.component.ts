import { Component, OnInit } from '@angular/core';
import { CalculatorService } from '../../../services/calculator.service';
import { DisplayComponent } from '../display/display.component';
import { KeyboardComponent } from '../keyboard/keyboard.component';
import { ActivatedRoute } from '@angular/router';



@Component({
  selector: 'app-calculator',
  standalone: true,
  imports: [DisplayComponent, KeyboardComponent],
  providers: [CalculatorService],
  templateUrl: './calculator.component.html',
  styleUrl: './calculator.component.scss',
})
export class CalculatorComponent implements OnInit{
  
  display = '';
  result = 0;

  constructor(private service: CalculatorService, ) { }
  ngOnInit(): void {

    this.service.display$.subscribe((display) => {
      this.display = display;
    });
    this.service.result$.subscribe((result) => {
      this.result = result;
    });
  }

  handleClick(value: number | string) {
    typeof value === 'string'
      ? this.service.handleString(value)
      : this.service.handleNumber(value);
  }

  
}
