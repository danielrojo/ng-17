import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Hero } from '../../../models/hero';

@Component({
  selector: 'app-heroes-list',
  standalone: true,
  imports: [],
  templateUrl: './heroes-list.component.html',
  styleUrl: './heroes-list.component.scss'
})
export class HeroesListComponent {

  @Input() heroes: Hero[] = [];
  @Output() onDeleteHero = new EventEmitter<number>();

  deleteHero(index: number): void {
    console.log('HeroesListComponent.deleteHero', index);
    this.onDeleteHero.emit(index);
  }
}
