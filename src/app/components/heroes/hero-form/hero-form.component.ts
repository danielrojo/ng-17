import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { Hero } from '../../../models/hero';
import { FormsModule } from '@angular/forms';
import { HeroesService } from '../../../services/heroes.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-hero-form',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './hero-form.component.html',
  styleUrl: './hero-form.component.scss',
})
export class HeroFormComponent implements OnInit, OnDestroy {
  heroName = '';
  heroDescription = '';

  constructor(
    private service: HeroesService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  ngOnInit(): void {
    this.heroName = this.service.formHero.name;
    this.heroDescription = this.service.formHero.description;
  }
  ngOnDestroy(): void {
    this.service.formHero = new Hero(this.heroName, this.heroDescription);
  }

  addHero() {
    console.log('Adding hero: ' + this.heroName);
    this.service.addHero(new Hero(this.heroName, this.heroDescription));
    this.heroName = '';
    this.heroDescription = '';
  }

  sendHero() {
    console.log('Sending hero: ' + this.heroName);
    this.router.navigate(['/calculator', new Hero(this.heroName, this.heroDescription)]);
    this.heroName = '';
    this.heroDescription = '';
  }
}
