import { JsonPipe, NgIf } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Hero } from '../../../models/hero';
import { HeroesService } from '../../../services/heroes.service';
import { HeroesListComponent } from '../heroes-list/heroes-list.component';
import { HeroFormComponent } from '../hero-form/hero-form.component';

@Component({
  selector: 'app-heroes',
  standalone: true,
  imports: [FormsModule, NgIf, JsonPipe, HeroesListComponent, HeroFormComponent],
  templateUrl: './heroes.component.html',
  styleUrl: './heroes.component.scss'
})
export class HeroesComponent implements OnInit, OnDestroy{

  heroes: Hero[] = [];

  constructor(private service: HeroesService) { 
  }

  ngOnInit(): void {
    console.log('HeroesComponent.ngOnInit');
    this.heroes = this.service.heroes;
  }

  ngOnDestroy(): void {
    console.log('HeroesComponent.ngOnDestroy');
  }

  deleteHero(index: number): void {
    this.service.deleteHero(index);
    this.heroes = this.service.heroes;
  }

}
