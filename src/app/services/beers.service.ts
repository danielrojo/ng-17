import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Beer } from '../models/beer';

@Injectable({
  providedIn: 'root'
})
export class BeersService {

  private _beers: Beer[] = [];

  beers$ = new BehaviorSubject<Beer[]>(this._beers);

  constructor(private http: HttpClient) { }

  getBeers() {
    const observer = {
      next: (data: any) => {
        console.log(data);
        data.forEach((element: any) => {
          this._beers.push(new Beer(element));
        });
        this.beers$.next([...this._beers]);
      },
      error: (error: any) => {
        console.error('ERROR: ' + error);
      },
      complete: () => {
        console.log('complete');
      },
    };
    this.http.get('https://api.punkapi.com/v2/beers').subscribe(observer);
  }

  getBeer(id: number | undefined) {
    return this._beers.find((beer: Beer) => beer.id === id);
  }
}
