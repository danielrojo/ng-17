import { Injectable } from '@angular/core';
import { Hero } from '../models/hero';

@Injectable({
  providedIn: 'root'
})
export class HeroesService {

  private _heroes = [ 
    new Hero('Windstorm', 'Windstorm is a superhero from the DC Universe. She is the daughter of the Golden Age hero Fury and the Greek god of the sky, Zeus. She possesses the powers of superhuman strength, flight, and the ability to control the weather.'),
    new Hero('Bombasto', 'Bombasto is a superhero from the DC Universe. He is a member of the Legion of Super-Heroes. He possesses the power of superhuman strength and the ability to fly.'),
  ];

  private _heroForm = new Hero('', '');

  constructor() { }

  get heroes() {
    return [...this._heroes];
  }

  get formHero() {
    return this._heroForm;
  }

  set formHero(hero: Hero) {
    this._heroForm = hero;
  }

  addHero(hero: Hero) {
    this._heroes.push(hero);
  }

  deleteHero(index: number) {
    this._heroes.splice(index, 1);
  }


}
