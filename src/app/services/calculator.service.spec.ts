import { TestBed } from '@angular/core/testing';

import { CalculatorService } from './calculator.service';

describe('CalculatorService', () => {
  let service: CalculatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CalculatorService],
    });
    service = TestBed.inject(CalculatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should add two numbers', () => {
    service.firstFigure = 1;
    service.secondFigure = 2;
    service.operator = '+';

    expect(service.resolve()).toBe(3);
  });

  it('should subtract two numbers', () => {
    service.firstFigure = 1;
    service.secondFigure = 2;
    service.operator = '-';

    expect(service.resolve()).toBe(-1);
  });

  it('should divide by zero', () => {
    service.firstFigure = 1;
    service.secondFigure = 0;
    service.operator = '/';

    expect(service.resolve()).toBe(Infinity);
  });

  it('should multiply two numbers', () => {
    service.firstFigure = 1;
    service.secondFigure = 2;
    service.operator = '*';

    expect(service.resolve()).toBe(2);
  });

  it('inifinity times zero', () => {
    service.firstFigure = Infinity;
    service.secondFigure = 0;
    service.operator = '*';

    // check if the result is NaN
    expect(service.resolve()).toBeNaN();
  });
});
