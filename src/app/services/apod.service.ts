import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Apod } from '../models/apod';

@Injectable()
export class ApodService {

  private _apod: Apod = new Apod();

  apod$ = new BehaviorSubject<any>(null);

  constructor(private http: HttpClient) {}

  private _processNext = (response: any) => {
    console.log(response);
    this._apod = new Apod(response);
    this.apod$.next(this._apod);
  }

  getApod(dateStr?: string) {

    let url = 'https://api.nasa.gov/planetary/apod?api_key=';
    url += 'DEMO_KEY';
    if (dateStr) {
      url += '&date=' + dateStr;
    }

    const observer = {
      next: (data: any) => {
        this._processNext(data);
        },
      error: (error: any) => {
        console.error('ERROR: ' + error);
      },
      complete: () => {
        console.log('complete');
      },
    };
    this.http
      .get(url)
      .subscribe(observer);
  }

}
