import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


enum State {
  init,
  firstFigure,
  secondFigure,
  result,
}

@Injectable()
export class CalculatorService {
  private _display = '';
  private result = 0;
  display$ = new BehaviorSubject<string>(this._display);
  result$ = new BehaviorSubject<number>(this.result);

  currentState = State.init;
  firstFigure = 0;
  secondFigure = 0;
  operator = '';

  constructor() { }

  handleString(value: string) {
    switch (this.currentState) {
      case State.init:
        break;
      case State.firstFigure:
        if (this.isOperator(value)) {
          this.operator = value;
          this.currentState = State.secondFigure;
          this._display += value
          this.display$.next(this._display);
        }
        break;
      case State.secondFigure:
        if (value === '=') {
          this.currentState = State.result;
          this.result = this.resolve();
          this.result$.next(this.result);
          this._display += value;
        }
        this.display$.next(this._display)
        break;
      case State.result:
        if (this.isOperator(value)) {
          let temp = this.result;
          this.clearCalculator();
          this.firstFigure = temp;
          this.operator = value;
          this.currentState = State.secondFigure;
          this._display = this.firstFigure + value;
          this.display$.next(this._display);
        }
        break;
    }
  }

  resolve(): number {
    switch (this.operator) {
      case '+':
        return this.firstFigure + this.secondFigure;
      case '-':
        return this.firstFigure - this.secondFigure;
      case '*':
        return this.firstFigure * this.secondFigure;
      case '/':
        return this.firstFigure / this.secondFigure;
      default:
        return 0;
    }
  }

  isOperator(value: string) {
    return value === '+' || value === '-' || value === '*' || value === '/';
  }

  handleNumber(value: number) {
    switch (this.currentState) {
      case State.init:
        this.firstFigure = value;
        this.currentState = State.firstFigure;
        this._display = value.toString();
        this.display$.next(this._display)
        break;
      case State.firstFigure:
        this.firstFigure = this.firstFigure * 10 + value;
        this._display += value.toString();
        this.display$.next(this._display)
        break;
      case State.secondFigure:
        this.secondFigure = this.secondFigure * 10 + value;
        this._display += value.toString();
        this.display$.next(this._display)
        break;
      case State.result:
        this.clearCalculator();
        this.firstFigure = value;
        this.currentState = State.firstFigure;
        this._display = value.toString();
        this.display$.next(this._display)
        break;
    }
  }

  clearCalculator() {
    this._display = '';
    this.currentState = State.init;
    this.display$.next(this._display)
    this.firstFigure = 0;
    this.secondFigure = 0;
    this.result = 0;
    this.result$.next(this.result);
  }
}