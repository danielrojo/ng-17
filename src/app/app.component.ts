import { Component, numberAttribute } from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { CalculatorComponent } from './components/calculator/calculator/calculator.component';
import { HeroesComponent } from './components/heroes/heroes/heroes.component';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { ApodComponent } from './components/apod/apod/apod.component';
import { CalculatorService } from './services/calculator.service';
import { BeersComponent } from './components/beers/beers/beers.component';
import { ApodService } from './services/apod.service';
import { FormsComponent } from './components/forms/forms/forms.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterModule, RouterOutlet, NgbNavModule, CalculatorComponent, HeroesComponent, ApodComponent, BeersComponent, FormsComponent],
  providers: [ApodService],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  active = 5;
}
